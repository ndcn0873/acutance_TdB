This repository contains Matlab code for the quanitification of vessel visibility, specifically designed for intracranial DANTE-SPACE acquisitions reconstructed in SNR units. Based on original code by Luca Biasiolli.

Please see acutance_TdB_demo.m for some more explanation and examples.

For further questions, please contact Matthijs de Buck:
matthijs.debuck@ndcn.ox.ac.uk / thijsdebuck@live.nl
