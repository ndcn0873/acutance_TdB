%% Demo for acutance_TdB
% Below are three examples for which the three acutance metrics (inner
% boundary acutance, outer boundary acutance, and RMS gradient) can be
% calculated. For each segments, the expected resulting values (based on
% the acutance calculations for each example by the authors) are provided 
% (using the Example_output.fig files) for comparison. Note that the 
% resulting values can vary slightly depending on the
% exact outline positioning (should be within a few %).
%
% For questions/comments, please contact:
% matthijs.debuck@ndcn.ox.ac.uk / thijsdebuck@live.nl
% Based on original code by Luca Biasiolli

% Three manual steps are required (on successive figures):
%         (NOTE: examples of the 3 steps are shown in Example_3_steps.pptx)
% 1) Drag an ROI surrounding the vessel segment (+at least ~10px around it)
%       (by clicking and dragging the blue area, double click to confirm)
% 2) On the next figure, indicate a rough outline of the vessel wall 
%       (by clicking on a few (5-10) locations, then hit 'enter')
% 3) On the next figure, first delineate the inner boundary on the unrolled
%       ("ribbon") figure to the right based on the detected gradient peaks 
%       indicated in black. Click on (typically) 10-15 locations (from left
%       to right) corresponding to gradient peaks. Hit enter, then do 
%       the same for the outer boundary (generally indicated in white, for 
%       VW-to-CSF contrast). Again, 10-15 locations typically.
%       Hit enter again --> done.

%% First, load some example data:
% Two simulated vessel segments with/without noise, and one in vivo vessel
% segment (which was used for the figures in the paper/thesis)
load('examples.mat')

%% Example 1: noise-free simulated vessel segment
acutance_TdB(example_sim_noNoise);
% For comparison, see Example1_output.fig

%% Example 2: simulated vessel segment with noise
acutance_TdB(example_sim_noise);
% For comparison, see Example2_output.fig

%% Example 3: in vivo vessel segment
acutance_TdB(example_vessel);
% For comparison, see Example3_output.fig


