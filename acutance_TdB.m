function [dRMSg, rms_in, rms_out, RMSgrad, inner_grad, outer_grad, max_SNRs] = acutance_TdB(input_image_all)
% [dRMSg, rms_in, rms_out, RMSgrad, inner_grad, outer_grad, max_SNRs] = acutance_TdB(input_image_all)
%
% This is a modified version of Luca Biasiolli's originial acutance.m
% function, aimed at improving its use for intracranial DANTE-SPACE
% acquisitions. It assumes input data in SNR units, and therefore doesn't
% perform any normalization.

subpixels = 0.25;       % interpolation variable used for subpixel accuracy
No_segments = 90;       % fixed number of radii 
angles = 0:(360/No_segments):359.99;
res_in = 0.23;          % in-plane resolution of input data (mm)

input_image_all = double(squeeze(input_image_all));

%% Get an estimate of the vessel ROI and centre location
figure, imagesc(input_image_all), axis image; colormap gray
caxis([0 prctile(input_image_all(:),95)])  
title('Select vessel ROI','FontSize',15)
input_image = imcrop;
close
if isempty(input_image), error('Invalid region'), end %% TdB catch

figure, imagesc(input_image), axis image; colormap gray
caxis([0 prctile(input_image_all(:),95)])
title('Indicate vessel outline','FontSize',15)
[xendo,yendo] = getline('closed');
[endo_mask] = roipoly(input_image,xendo,yendo);
hold on;
plot(xendo,yendo,'k-');

[y_posn,x_posn] = ndgrid(1:size(endo_mask,1),1:size(endo_mask,2));
pixels_within = find(endo_mask);
centre_x = mean(x_posn(pixels_within));
centre_y = mean(y_posn(pixels_within));
plot(centre_x,centre_y, 'r*');
 
%% Draw a line which is the No_segments o'clock line of the No_segments segment_vessel
% TdB remove: make automatic using a guesstimate (0 degree edge of
% previously drawn vessel outline)
angleNo_segments = 0;

radius = sqrt(max(abs(yendo-centre_y)).^2 + max(abs(xendo-centre_x)).^2);

radial_points = floor(1.12*radius/subpixels);
x_posns = zeros(radial_points, No_segments);
y_posns = zeros(radial_points, No_segments);
counter = 1 : No_segments;
angle = angleNo_segments - ((counter-1) * 2.0 * pi / No_segments);
close %TdB close center-identification figure

%% Extract & show line profiles
x_posns(:,:) = centre_x + subpixels*(0:radial_points-1)'*sin(angle);
y_posns(:,:) = centre_y + subpixels*(0:radial_points-1)'*cos(angle);

image_posnx = [1:size(input_image,1)];
image_posny = [1:size(input_image,2)];
image_posnx = image_posnx' * ones(1, size(input_image,2));
image_posny = ones(size(input_image,1),1) * image_posny;
radial_image = griddata(image_posnx, image_posny, input_image, y_posns, x_posns, 'linear'); %{'Qt','Qbb','Qc','Qz'});
duplicated = [radial_image radial_image];

figure('Position', [100 100 1400 700])
subplot(1,2,1)
imagesc(input_image), axis image, hold on
plot(x_posns, y_posns,'y-')
plot(xendo, yendo,'r--', 'LineWidth', 2)
title('Source image')
xlim([min(x_posns(:)) max(x_posns(:))])
ylim([min(y_posns(:)) max(y_posns(:))])
hold off
labels = 0:90:270;
set(gca,'FontSize',14)

%% Also show gradient ribbon
% Create a function for the convolution, we need to include a length scaling at this radius so that the function obeys the Canny criteria
subplot(1,2,2)
size_of_grid = 5; %this is actually not the size at all (*2)!
[x,y] = meshgrid(-size_of_grid:size_of_grid,-size_of_grid:size_of_grid);

% This creates an edge detector for the x-direction.
alpha_x = 0.75;
alpha_y = 0.75;
filter_function = alpha_x*x.*exp(-alpha_x.*abs(x)).*(1+alpha_y.*abs(y)).*exp(-alpha_y.*abs(y));

% The transpose of the filter function is used here so it detects in the y (radial) direction
edge_detected = conv2(duplicated,filter_function','same');
h = imagesc(edge_detected(1:end-5,No_segments/2+(1:No_segments))); %TdB reduced to single copy (not duplicate) 
colormap gray, hold on
cmap_seg = edge_detected(5:end-8,No_segments:-1:1);
caxis([prctile(cmap_seg(:),5),prctile(cmap_seg(:),95)])

% Add initial outline as a guide to the eye
[theta,rho] = cart2pol(xendo-centre_x,yendo-centre_y);
theta = mod(pi/2+theta,2*pi);
[theta, ind] = sort(theta); rho = rho(ind);
theta = [theta(end)-2*pi; theta; theta(1)+2*pi];
rho = [rho(end); rho; rho(1)];
plot(theta./(2*pi)*No_segments+1,rho/subpixels,'r--')

set(get(h,'Parent'),'XTickLabel', labels);
set(get(h,'Parent'),'XTick', [0 1 2 3] * round(No_segments)/length(labels));
title('Gradient (black/white = inner/outer boundaries)');
xlabel('Angle (�)') 
ylabel('Radial position (interpolated)') 
set(gca,'FontSize',14)

%% Manual positioning of boundary for dark blood images (inner/outer walls)
for inner_flag = 1:2
    %Get the boundary
    [angular_position_init, radius_init] = getpts;
    angular_position = mod(angular_position_init*(-1)+No_segments/2,No_segments); %workaround to fix the indexing (to align previous figures' notation to the final notation)
    
    % The code was written for 2 copies of the vessel segments; however,
    % I reduced this to a single copy to reduce the required amount of
    % manual input, so for calculations we need to add a 2nd copy
    radius = [radius_init; radius_init+rand(size(radius_init))*0.2-0.1];   %TdB added (random variation to prevent overlapping values for interpolation)
    angular_position = [angular_position; angular_position + No_segments];   %TdB added
    
    % Sort the data into order to avoid disappointment
    [angular_position, index] = sort(angular_position);
    radius = radius(index);
    
    % Just introduce a little jitter to avoid interp1 errors
    angular_position = angular_position + randn(length(angular_position),1)*0.00001;
    minimum_angle = ceil(min(angular_position*(360/No_segments)));
    maximum_angle = floor(max(angular_position*(360/No_segments)));
    
    interped_angles = (minimum_angle:maximum_angle)*pi/180;
    try
        radius_2 = (interp1(((angular_position*(2*pi/No_segments))), radius, interped_angles, 'linear'));
    catch
        disp('Problem with interp1');
    end
    x_points = centre_x + subpixels*radius_2.*sin((minimum_angle: maximum_angle)*2*pi/360); %TdB changed sign to agree with original data
    y_points = centre_y + subpixels*radius_2.*cos(-(minimum_angle: maximum_angle)*2*pi/360); %TdB changed sign to agree with original data
    
    subplot(1,2,1), hold on
    plot(x_points, y_points,'g-'), hold off
    subplot(1,2,2), hold on
    plot(angular_position_init, radius_init,'g-'), hold off
    
    % Now set it up so that the angular range is 0:2*pi
    all_angles = mod(interped_angles,2*pi);
    [~, index] = sort(all_angles);
    radius_2 = radius_2(index);
    if (inner_flag == 1)
        region.inner_x_points = x_points;
        region.inner_y_points = y_points;
        inner_rad = radius;
        inner_ang = angular_position;
    else
        region.outer_x_points = x_points;   %TdB changed (fixed?)
        region.outer_y_points = y_points;   %TdB changed
        outer_rad = radius;
        outer_ang = angular_position;
    end
end

% Calculate the thickness as a function of position
diff_data = analyse_curves(region);                                     % sometimes it produces rough contours!

%% Prepare a ribbon
radial_offset = max(round((diff_data.mean_radius - diff_data.mean_thickness)/subpixels) - 28, 1);   %TdB Test: extended the boundaries to allow for large asymmetries (e.g. in CoW area for intracranial DANTE-SPACE)
radial_points = min(round(3*diff_data.mean_thickness/subpixels)+85, size(duplicated,1));

quarto = size(duplicated,2)/4;
ribbon = duplicated(radial_offset:min(radial_offset+radial_points,size(duplicated,1)),quarto+2:3*quarto+1); %TdB modified

rad_lim_in = inner_rad(1)/2+inner_rad(end)/2;  %TdB fix for limits
inner_contour1 = interp1([0;inner_ang; 180], [rad_lim_in; inner_rad; rad_lim_in], 1:2*No_segments, 'linear');
inner_contour = round(inner_contour1(quarto+1:3*quarto)- radial_offset); %TdB modified 
inner_contour = flip(circshift(inner_contour,1,2),2); %TdB modified

rad_lim_out = outer_rad(1)/2+outer_rad(end)/2;  %TdB fix for limits
outer_contour1 = interp1([0; outer_ang; 180], [rad_lim_out;outer_rad;rad_lim_out], 1:2*No_segments, 'linear');
outer_contour = round(outer_contour1(1+quarto:3*quarto)- radial_offset); %TdB modified
outer_contour = flip(circshift(outer_contour,1,2),2); %TdB modified

contour_thickness = outer_contour - inner_contour;
% profile_len = floor(min(contour_thickness)/2);
profile_len = 2;    %TdB overruled/fixed for DANTE-SPACE acquisitions
inizio = floor(max(inner_contour - (contour_thickness/2), 1));  %Why not go Italian :-)
fine = ceil(min(outer_contour + (contour_thickness/2), (size(ribbon,1)-1)));

% TdB in: Highly elongated vessels cannot properly be delineated using the
% default approach. It'd be a pity to discard all values though, so instead
% I'll remove the values at very large radii, such that we can still
% include all datapoints in the relialy 'delineatable' part of the vessel.
limR = 150;
remove_vals = ~((inizio < limR) .* (fine < limR));
remove_vals(1:end-1) = max(remove_vals(1:end-1),remove_vals(2:end));
remove_vals(2:end) = max(remove_vals(2:end),remove_vals(1:end-1));
inizio(remove_vals) = NaN;
fine(remove_vals) = NaN;
No_segments_eff = No_segments-sum(remove_vals); %effective number of radial values

remove_vals_interp = zeros(1,No_segments/subpixels);
for i = 1:numel(remove_vals_interp)
    remove_vals_interp(i) = remove_vals(ceil(i*subpixels));
end
remove_vals_interp = logical(remove_vals_interp);
remove_vals_interp = circshift(remove_vals_interp,-numel(remove_vals_interp)/4);
% TdB out

%% TdB: Prep output figure; include img of vessel segment + outlines
diff_data.x_new_in(remove_vals_interp) = NaN;
diff_data.y_new_in(remove_vals_interp) = NaN;
diff_data.x_new_out(remove_vals_interp) = NaN;
diff_data.y_new_out(remove_vals_interp) = NaN;

close
figure('Position', [200 200 1400 400])
subplot(1,3,1)
imagesc(input_image), 
caxis([min(input_image(:)) prctile(input_image(:),95)])
hold on
plot(diff_data.x_new_in, diff_data.y_new_in,'g-')
plot(diff_data.x_new_out, diff_data.y_new_out,'r-')
plot([1 1]*mean(diff_data.x_new_in), [mean(diff_data.y_new_in), min(diff_data.y_new_in)-5], 'y-')
xlim([min(diff_data.x_new_out) max(diff_data.x_new_out)] + [-20 20])
ylim([min(diff_data.y_new_out) max(diff_data.y_new_out)] + [-20 20])
axis equal off, colormap gray
title('Image segment & outline')
set(gca,'FontSize',15)

%% Measure directional RMS gradient (Mudigonda 2000)
% TdB Note: removed normalization, since the input data (in SNR units) has
% sensible units, additional normalization limits the direct comparison
% between different acquisitions or field strengths.
% (hence also removed the 'dmin' and 'dmax' parameters)
real_rib = zeros(size(ribbon));
RMSgrad = zeros(1,No_segments);
for i = 1:No_segments
    if ~remove_vals(i)
        real_rib(inizio(i):fine(i),i) = ribbon(inizio(i):fine(i),i);
    end
end        
                                                                           
dsgrad = diff(ribbon,1,1).^2;
for i = 1:No_segments 
    if ~remove_vals(i)
        RMSgrad(i) = sqrt((sum(dsgrad(inizio(i):fine(i),i)))/(fine(i)-inizio(i)+1));
    else
        RMSgrad(i) = NaN;
    end
end

%TdB: change values to final contrast-per-mm units
RMSgrad = RMSgrad / subpixels / res_in;

%TdB modified (no need for normalization w/SNR units) --> deviates from the
%normalized equation used by Mudigonda et al
dRMSg = nansum(RMSgrad)/No_segments_eff;    %directional RMS gradient

subplot(1,3,3),   %TdB no new fig
plot(angles,RMSgrad, 'LineWidth', 2),title('Gradient'), 
box on, axis square
xlabel('Angle (�)')
ylabel('G_{RMS} (\DeltaSNR/mm)')
xlim([0 360])
ylim([0 max(RMSgrad)*1.05])
legend('RMS = '+string(round(dRMSg,1)),'Location', 'south')
set(gca,'FontSize',15)

%% Measure of image edge-profile acutance (Rangayyan 1995-7, Olabarriaga 1996)
avg_grad = zeros(1,No_segments);
inner_grad = zeros(1,No_segments);
outer_grad = zeros(1,No_segments);

for i = 1:No_segments
    if ~remove_vals(i)
        inner_d = zeros(1,No_segments);
        outer_d = zeros(1,No_segments);
        for j = 1:profile_len
            inner_d(j) = (ribbon(inner_contour(i)+j,i) - ribbon(max(inner_contour(i)-j,1),i))/(2*j);    %TdB fixed: /(2*j) instead of /2*j
            outer_d(j) = (ribbon(outer_contour(i)-j,i) - ribbon(min(outer_contour(i)+j,size(ribbon,1)),i))/(2*j);   %TdB fixed: /(2*j) instead of /2*j
        end
        avg_grad(i) = (sum(inner_d) + sum(outer_d))/profile_len;
        inner_grad(i) = sum(inner_d)/profile_len;
        outer_grad(i) = sum(outer_d)/profile_len;

        % TdB edit: overwrite this; for us, using the 2nd value (gradient over
        % subpix's -2 to +2) is more representative of the underlying,
        % un-interpolated, CNR/px in the input image. Note this 2nd value is
        % only the correct one when using subpixels = 0.25.
        avg_grad(i) = inner_d(2)+outer_d(2);
        inner_grad(i) = inner_d(2);
        outer_grad(i) = outer_d(2);
    else
        avg_grad(i) = NaN;
        inner_grad(i) = NaN;
        outer_grad(i) = NaN;
    end
end

%TdB: use \Delta SNR/mm values, correcting for bicubic interpolation
inner_grad = inner_grad/subpixels/res_in;
outer_grad = outer_grad/subpixels/res_in;

subplot(1,3,2),   %TdB no new fig 
title('Boundary acutance'), hold on, box on, axis square
plot(angles, inner_grad,'g', 'LineWidth', 2), 
plot(angles, outer_grad,'r', 'LineWidth', 2), 
plot([0 360], [0 0], 'k--'), hold off
xlabel('Angle (�)'), ylabel('CNR/mm')  
xlim([0 360])
ylim([min([inner_grad,outer_grad])-1.5 max([inner_grad,outer_grad])+.5])
rms_in = rms(inner_grad(~remove_vals)); rms_out = rms(outer_grad(~remove_vals));
legend('Inner (RMS = '+string(round(rms_in,1))+')', ...
    'Outer (RMS = '+string(round(rms_out,1))+')','Location', 'south')
set(gca,'FontSize',15)

%% Finally: calculate max_SNR data (not used for DANTE-SPACE project)
max_SNRs = zeros(No_segments,1);
for i = 1:No_segments
    max_SNRs(i) = max(ribbon(inner_contour:outer_contour,i));
end
end

function diff_data = analyse_curves(region)

x_points_in = region.inner_x_points;
y_points_in = region.inner_y_points;

x_points_out = region.outer_x_points;
y_points_out = region.outer_y_points;

length_in = length(x_points_in);
length_out = length(x_points_out);
centre_x = mean([reshape(x_points_in, 1, length_in), reshape(x_points_out, 1, length_out)]);
centre_y = mean([reshape(y_points_in, 1, length_in), reshape(y_points_out, 1, length_out)]);

r_points_in = sqrt((x_points_in - centre_x).^2+(y_points_in - centre_y).^2);
r_points_out = sqrt((x_points_out - centre_x).^2+(y_points_out - centre_y).^2);

t_points_in = atan2(y_points_in - centre_y, x_points_in - centre_x);
t_points_out = atan2(y_points_out - centre_y, x_points_out - centre_x);

[~, index] = sort(t_points_in);
len = length(r_points_in);
t_sorted_in = t_points_in([index(len); reshape(index,len,1); index(1)]);
t_sorted_in(1) = t_sorted_in(1) - 2*pi;
t_sorted_in(len+2) = t_sorted_in(len+2) + 2*pi;
r_sorted_in = r_points_in([index(len); reshape(index,len,1); index(1)]);

[~, index] = sort(t_points_out);
len = length(r_points_out);
t_sorted_out = t_points_out([index(len); index([1:len])'; index(1)]);
t_sorted_out(1) = t_sorted_out(1) - 2*pi;
t_sorted_out(len+2) = t_sorted_out(len+2) + 2*pi;
r_sorted_out = r_points_out([index(len); index'; index(1)]);

t_new_in = [-179:180]*pi/180;
r_new_in = interp1(t_sorted_in, r_sorted_in, t_new_in,  'linear');

t_new_out = [-179:180]*pi/180;
r_new_out = interp1(t_sorted_out, r_sorted_out, t_new_out,  'linear');

x_new_in = r_new_in .* cos(t_new_in) + centre_x;
y_new_in = r_new_in .* sin(t_new_in) + centre_y;

x_new_out = r_new_out .* cos(t_new_out) + centre_x;
y_new_out = r_new_out .* sin(t_new_out) + centre_y;

mean_r_in = mean(r_new_in);
x_double = 2* mean_r_in .* cos(t_new_in) + centre_x;
y_double = 2* mean_r_in .* sin(t_new_in) + centre_y;

thickness = r_new_out - r_new_in;
max_thickness = max(thickness);
min_thickness = min(thickness);
mean_thickness = mean(thickness);

pos_max_thickness = find(thickness==max_thickness);
pos_max_thickness = pos_max_thickness(1);
pos_min_thickness = find(thickness==min_thickness);
pos_min_thickness = pos_min_thickness(1);

min_line = [x_new_in(pos_min_thickness), y_new_in(pos_min_thickness); ...
        x_new_out(pos_min_thickness), y_new_out(pos_min_thickness)];
max_line = [x_new_in(pos_max_thickness), y_new_in(pos_max_thickness); ...
        x_new_out(pos_max_thickness), y_new_out(pos_max_thickness)];

diff_data.r_new_in = r_new_in;
diff_data.t_new_in = t_new_in;
diff_data.r_new_out = r_new_out;
diff_data.t_new_out = t_new_out;

diff_data.x_new_in = x_new_in;
diff_data.y_new_in = y_new_in;
diff_data.x_new_out = x_new_out;
diff_data.y_new_out = y_new_out;
diff_data.x_double = x_double;
diff_data.y_double = y_double;
diff_data.centre_x = centre_x;
diff_data.centre_y = centre_y;

diff_data.mean_thickness = mean_thickness;
diff_data.pos_max_thickness = pos_max_thickness;
diff_data.pos_min_thickness = pos_min_thickness;
diff_data.min_line = min_line;
diff_data.max_line = max_line;

index1 = [-179:180];
for ii=1:180
    same_line = find(index1==ii | index1==ii+180 | index1==ii-180);
    if(length(same_line)~=2)
        disp('error');
    end
    diff_data.diameters(ii) = sum(diff_data.r_new_in(same_line));
end
diff_data.obliqueness = max(diff_data.diameters)/min(diff_data.diameters);
diff_data.mean_radius = mean(diff_data.diameters)/2;

end
